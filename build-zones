#!/usr/bin/perl
# vim:set ai et sts=4 sw=4 tw=4 tw=0:
# Local Variables:
# mode:cperl
# cperl-indent-level:4
# End:

# Copyright © Stephen Gran 2009
# Copyright (c) 2009 Peter Palfrader
#
# Author: Stephen Gran <steve@lobefin.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, under version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

use strict;
use warnings;
use YAML;
use POSIX qw(strftime);
use File::Path;
use English;
use Getopt::Long;
use File::Basename;
use File::Temp qw(tempfile);
use Data::Compare;
use Digest::SHA;

use FindBin;
use lib "$FindBin::Bin";
use lib "$FindBin::Bin/../dns-helpers";

use DSA::DNSHelpers;


my $USAGE = "$PROGRAM_NAME: Usage: $PROGRAM_NAME [--help] [--force] [--print-filenames-only|--create-named-conf] [--hostsfile=<file>] <zonedir>\n";
my $PARAMS;
if (!GetOptions (
    '--help'                 => \$PARAMS->{'help'},
    '--hostsfile=s'          => \$PARAMS->{'hostsfile'},
    '--status-dir=s'         => \$PARAMS->{'statusdir'},
    '--print-filenames-only' => \$PARAMS->{'print-filenames-only'},
    '--create-named-conf'    => \$PARAMS->{'create-named-conf'},
    '--force'                => \$PARAMS->{'force'},
    )) {
    die($USAGE);
};
die "$USAGE" if (@ARGV != 1);
my $ZONEDIR = shift;

my $STATUSDIR = $PARAMS->{'statusdir'} || 'status';
my $HOSTSFILE = $PARAMS->{'hostsfile'} || 'hosts.yaml';


my $RECVCONF_TEMPLATE = <<'EOF';
file @@FILE@@
    perms 0644
    user geodnssync
    group geodnssync
    precommand /usr/sbin/named-checkzone -q -k fail -n fail -S fail -i full -m fail -M fail @@TOPZONE@@ @@FILE@@
    postcommand sudo /etc/init.d/bind9 reload
EOF
if ($PARAMS->{'help'}) {
    print $USAGE;
    print "builds bind zonefiles for Debian's geodns setup.\n";
    exit (0);
};


my $CONFIG = load_config(qw{outdir-geo statedir-geo keydir default_ttl soa bindzonedir-geo});
my $OUTDIR = $CONFIG->{'outdir-geo'};


my $ALIVE_CACHE = {};
sub check_host {
    my $service = shift;
    my $host = shift;

    return $ALIVE_CACHE->{$service}->{$host} if (defined $ALIVE_CACHE->{$service} &&
                                                 defined $ALIVE_CACHE->{$service}->{$host});

    my $res_ok = !( -e "$STATUSDIR/$host" || -e "$STATUSDIR/$host-$service");

    $ALIVE_CACHE->{$service} = {} unless defined $ALIVE_CACHE->{$service};
    $ALIVE_CACHE->{$service}->{$host} = $res_ok;
    return $res_ok;
}

sub build_zone {
    my $topzone     = shift;
    my $hostref     = shift;
    my $zoneconfref = shift;
    my $zoneref     = shift;
    my $fallbackref = shift;
    my $hail_mary   = shift;

    $fallbackref    = $hail_mary   unless defined($fallbackref);
    $zoneref        = $fallbackref unless defined($zoneref);
    die("build_zone called without proper zone refs") unless defined $zoneref;

    my $srvcount = 0;
    my $ret;
    for my $proto (qw(4 6)) {
        for my $srv (@{$zoneref}) {
            next unless (defined($hostref->{hosts}->{$srv}->{$proto}));
            next unless (check_host($topzone, $srv));
            for my $addr (split(/\s*,\s*/, $hostref->{hosts}->{$srv}->{$proto})) {
                my $rr_type = ($proto == 4 ? 'A' : 'AAAA');
                $ret .= sprintf("\@\t\t%s\tIN\t%s\t%-37s\t; %s\n",
                                get_ttl($zoneconfref, $rr_type),
                                $rr_type,
                                $addr,
                                $srv);
            }
            $srvcount++;
        }
    }


    if ($srvcount == 0) {
        return build_zone($topzone, $hostref, $zoneconfref, $fallbackref, $hail_mary) if (defined $fallbackref or defined $hail_mary);
        die "No active servers found for default(==fallback) zone while building $topzone\n";
    }

    return $ret;
}

sub create_filelist {
    my $top_zone = shift;
    my $zoneref = shift;

    my $res = {};
    for my $zone (keys %{$zoneref->{'geozones'}}) {
        next if ($zone eq 'hail_mary');
        $res->{$zone} = $OUTDIR.'/'.($zone eq 'default' ? "db.$top_zone" : "db.$top_zone.$zone");
    };
    return $res;
}

open (F, '<', $HOSTSFILE) or die "Arggh: hosts file: $!\n";
my ($hostref, undef, undef) = Load(join "", (<F>));
close F;

unless ($PARAMS->{'create-named-conf'} || $PARAMS->{'print-filenames-only'}) {
    mkpath($OUTDIR);
    mkpath($CONFIG->{'statedir-geo'});
}

my $VIEWS;

sub update_view_mapping {
    my $top_zone = shift;
    my $outfiles = shift;
    my $zoneconfref = shift;

    unless (defined $VIEWS) {
        # The first geozone we open defines the list of views we have
        $VIEWS = {};
        for my $view (keys %$outfiles) {
            next if ($view eq 'hail_mary');
            $VIEWS->{$view} = {};
        };
    } else {
        # other zones must share the same views, else bind will fall over
        my @extra_views = ();
        for my $view (keys %$outfiles) {
            push(@extra_views, $view) unless defined $VIEWS->{$view};
        };
        die("$top_zone has extra view(s) (not found in the first zone): ", join(',', @extra_views)) if (scalar @extra_views);
    };
    for my $view (keys %$VIEWS) {
        next if ($view eq 'hail_mary');
        my $zonefile = $outfiles->{$view};
        die("No outfile for $top_zone:$view?") unless defined $zonefile;
        $VIEWS->{$view}->{$top_zone} = $zonefile;
    };
}

sub get_ttl {
    my $zoneconfref = shift;
    my $rr_type = lc(shift);

    my $defaults = { 'a' => 300, 'aaaa' => 300 };
    # for all others, do not override the default_ttl we
    # get from /etc/dns-helpers.yaml or whereever
    # load_config() gets its stuff from.
    my $ret = $defaults->{$rr_type} || '';

    if (defined $zoneconfref->{'ttl'} &&
        defined $zoneconfref->{'ttl'}->{$rr_type}) {
        $ret = $zoneconfref->{'ttl'}->{$rr_type}
    };

    return $ret;
}

sub prepare_zonedata {
    my $top_zone = shift;
    my $outfiles = shift;
    my $zoneconfref = shift;

    my $allgeozones = {};

    for my $geozone (keys %$outfiles) {
        if ($PARAMS->{'print-filenames-only'}) {
            print "$outfiles->{$geozone}\n";
            next;
        };

        die "Zone $geozone in list, but no way to build it\n" unless
                            (defined($zoneconfref->{geozones}->{$geozone}) ||
                             defined($zoneconfref->{geozones}->{default}));
        $allgeozones->{$geozone} = [];

        # write out NS and MX records
        for my $rr (qw(ns mx)) {
            for my $srvc (($rr eq 'ns' ? @{$zoneconfref->{$rr}} : keys %{$zoneconfref->{$rr}})) {
                push @{$allgeozones->{$geozone}},
                        sprintf ("\@\t%s\tIN\t%s\t%s%s\n",
                                get_ttl($zoneconfref, $rr),
                                 uc $rr,
                                ($rr eq 'mx' ? "$zoneconfref->{$rr}->{$srvc}\t" : ''),
                                ($srvc =~ /\.$/ ? $srvc : "$srvc."));
            }
        }
        # write out SRV records
        for my $srvc (keys %{$zoneconfref->{srv}}) {
            push @{$allgeozones->{$geozone}},
                    sprintf ("_%s._%s\t%s\tIN\tSRV\t%s %s %s\t%s\n",
                            $zoneconfref->{srv}->{$srvc}->{service},
                            $zoneconfref->{srv}->{$srvc}->{proto},
                            get_ttl($zoneconfref, "srv"),
                            $zoneconfref->{srv}->{$srvc}->{priority},
                            $zoneconfref->{srv}->{$srvc}->{weight},
                            $zoneconfref->{srv}->{$srvc}->{port},
                            ($srvc =~ /\.$/ ? $srvc : "$srvc."));
        }
        # a TXT record explaining which zone view this is
        push @{$allgeozones->{$geozone}}, sprintf ("\n\@\t%s\tIN\tTXT\t\"%s view\"\n\n", get_ttl($zoneconfref, 'TXT'), $geozone);
        if (defined $zoneconfref->{'include'}) {
            for my $incl (@{$zoneconfref->{'include'}}) {
                my $sha = Digest::SHA->new('sha256');
                $sha->addfile($incl);
                my $hex = $sha->hexdigest;
                push @{$allgeozones->{$geozone}}, "; file $incl; digest: $hex\n";
                push @{$allgeozones->{$geozone}}, "\$INCLUDE \"$incl\"\n";
            }
        }
        # and the zone data itself.
        push @{$allgeozones->{$geozone}},
                build_zone($top_zone, $hostref, $zoneconfref,
                            $zoneconfref->{geozones}->{$geozone}, # may not exist - that's fine, build_zone() handles that
                            $zoneconfref->{geozones}->{default},
                            $zoneconfref->{geozones}->{hail_mary});
    };

    return $allgeozones;
}

# compare the zonedata in the zd hash with the data
# stored from the last build.
# returns 0 on differences, 1 if there are none.
sub compare_zonedata {
    my $zd = shift;
    my $top_zone = shift;

    my $filename = "$CONFIG->{'statedir-geo'}/last.$top_zone.yaml";
    return 0 if ( ! -e $filename );

    open (F, '<', $filename) or die "Cannot open $filename: $!\n";
    my ($oldzd, undef, undef) = Load(join "", (<F>));
    close F;

    return Compare($zd, $oldzd);
};

sub store_zonedata {
    my $zd = shift;
    my $top_zone = shift;

    my $filename = "$CONFIG->{'statedir-geo'}/last.$top_zone.yaml";
    safe_write($filename, $zd);
};

sub write_zonefiles {
    my $top_zone = shift;
    my $outfiles = shift;
    my $zoneconfref = shift;

    my $zd = prepare_zonedata($top_zone, $outfiles, $zoneconfref);
    return if ($PARAMS->{'print-filenames-only'});

    my $alloutputfilespresent = 1;
    for my $geozone (keys %$zd) {
        unless (-e $outfiles->{$geozone}) {
            $alloutputfilespresent = 0;
            last;
        };
    };
    my $compare_hash = { 'in' => $zoneconfref, 'res' => $zd };
    return if (!$PARAMS->{'force'} && compare_zonedata($compare_hash, $top_zone) && $alloutputfilespresent);
    my $compare_dump = Dump($compare_hash);

    my $serial = new_serial($top_zone, $CONFIG->{'statedir-geo'});

    for my $geozone (keys %$zd) {
        my ($fd, $tmpzonefilename) = tempfile("$top_zone-$geozone.assemble-XXXXXX", DIR => $OUTDIR, SUFFIX => '.tmp');
        print $fd generate_zoneheader($serial, ttl => $CONFIG->{'default_ttl'}, %{$CONFIG->{'soa'}}, %{$zoneconfref->{'soa'}});
        print $fd join("\n", @{$zd->{$geozone}});
        close $fd;
        chmod(0664, $tmpzonefilename);

        if ($zoneconfref->{'dnssec'}) {
            my $ret = sign_zonefile($top_zone, $tmpzonefilename, %$CONFIG, %{$zoneconfref->{'dnssec-opts'}});

            if (!defined $ret) {
                    unlink $tmpzonefilename;
                    die("ERROR: dnssigner returned errors on zone $top_zone - aborted update.\n");
            };
        }
        if (! check_zonefile($top_zone, $tmpzonefilename)) {
            unlink $tmpzonefilename;
            die("ERROR: checkzone returned errors on zone $top_zone - not updated.\n");
        }
        rename $tmpzonefilename, $outfiles->{$geozone} or die "Cannot rename $tmpzonefilename to $outfiles->{$geozone}: $!\n";
    }
    store_zonedata($compare_dump, $top_zone);
}


opendir(DIR, "$ZONEDIR") or die ("Cannot opendir $ZONEDIR: $!\n");
for my $dirent (readdir(DIR)) {
    next if $dirent =~ /^\./;
    next unless $dirent =~ /\.zone$/;

    my $top_zone = basename($dirent, ".zone");
    my $filename = "$ZONEDIR/$dirent";

    open (F, '<', $filename) or die "Arggh: yamlfile $filename: $!\n";
    my ($zoneconfref, undef, undef) = Load(join "", (<F>));
    close F;

    my $outfiles = create_filelist($top_zone, $zoneconfref);

    # we need to keep a view/zone/file mapping for writing the bind config later on, if we have --create-named-conf
    if ($PARAMS->{'create-named-conf'}) {
        update_view_mapping($top_zone, $outfiles, $zoneconfref);
    } else {
        write_zonefiles($top_zone, $outfiles, $zoneconfref);
    };
}

sub view_header {
    my $view = shift;

    my $match = $view eq 'default' ? 'any' : $view;
    return <<EOF
view "$view" {
  match-clients { $match; };

EOF
}

if ($PARAMS->{'create-named-conf'}) {
    print "// This file was created by auto-dns/build-zones --create-named-conf\n";
    print "// and is distributed to hosts using puppet as\n";
    print "// dsa-puppet/modules/geodns/files/common/named.conf.geo\n";
    print "// you probably do not want to edit it manually wherever you find it\n";
    print "\n";

    # get a list of views, with default being the last.
    my @views = sort {$a cmp  $b} keys %$VIEWS;
    @views = grep {$_ ne 'default'} @views;
    push @views, 'default';

    for my $view (@views) {
        print view_header($view);
        for my $zone (sort {$a cmp $b} keys %{$VIEWS->{$view}}) {
            my $fn = $VIEWS->{$view}->{$zone};
            $fn = $CONFIG->{'bindzonedir-geo'}.'/'.basename($fn);

            print <<EOF;
  zone "$zone" {
    type master;
    file "$fn";
    notify no;
    allow-query { any; };
    allow-transfer { };
  };

EOF
        }
        print <<EOF;
  zone "29.172.in-addr.arpa" {
    type slave;
    notify no;
    file "db.29.172.in-addr.arpa-$view";
    masters {
      82.195.75.91; // denis
      2001:41b8:202:deb:1b1b::91; // denis
    };
    allow-query { any; };
    allow-transfer { };
  };
EOF
        print "};\n"; # end of view
    };
};
