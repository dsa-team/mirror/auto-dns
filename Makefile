SHELL=/bin/bash
HOSTS = geo1.debian.org geo2.debian.org geo3.debian.org
# hmm.  Adding new zones will not immediately get caught here, since they only
# get added by git pull, which happens after this wildcard expansion.  Hmmmm.
ZONEYAML = $(wildcard zones/*.zone)
FILES = $(shell ./zonebuilder --print-filenames-only zones)

IDENTITY=/srv/dns.debian.org/geo/home/.ssh/id_geodnssync
REMOTEUSER=geodnssync
RUNAS=geodnssync
EXTRADEPENDENCIES=/srv/dns.debian.org/puppet-extra/include-debian.org /srv/letsencrypt.debian.org/var/hook/snippet

all: install

pull:
	git pull > /dev/null

$(FILES): hosts.yaml $(ZONEYAML) status zones $(EXTRADEPENDENCIES)
	mkdir -p stamp
	@echo -e "\n\n\nBuilding zones\n\n\n"
	lockfile -r 4 -l 300 generate.lock && \
		if ./zonebuilder zones ; then \
			rm -f generate.lock; exit 0; \
		else \
			rm -f generate.lock; exit 1; \
		fi
	touch $(FILES)
	@echo -e "\n\n\ndone.\n\n\n"

clean:
	rm -rf generated
	rm -rf stamp/*

install: stamp/install-security

stamp/install-security: pull $(FILES)
	[ "$$(id -u '$(RUNAS)')" = "$$(id -u)" ] || (echo "Run as geodnssync"; exit 1)
	mkdir -p stamp
	md5sum -c $@ >/dev/null && touch $@ || \
	(       unset SSH_AUTH_SOCK && export SSH_AUTH_SOCK && \
		children="" && \
		for host in $(HOSTS); do \
			echo -n "Doing $$host ..."; \
			ssh -T -i "$(IDENTITY)" "$(REMOTEUSER)"@$$host & \
			pid="$$!"; \
			echo "$$pid"; \
			children="$$children $$pid"; \
		done && \
		for pid in $$children; do \
			echo "Waiting for $$pid ..." && \
			wait $$pid && echo "$$pid done." && continue; \
			\
			echo >&2 -e "\n\nPUSHING TO ONE HOST FAILED (pid $$pid)!\n\n"; \
			exit 1; \
		done && \
		echo "Updating md5sums in $@" && \
		md5sum generated/* > $@ \
	)

update-puppet:
	[ -e ../dsa-puppet ] || ( echo "expected puppet in ../dsa-puppet"; exit 1)
	[ -e ../dsa-puppet/modules/named/files/common/named.conf.geo ]
	./zonebuilder --create-named-conf zones > ../dsa-puppet/modules/named/files/common/named.conf.geo
	@echo -e "\nDon't forget to commit and push the changes to the puppet config."

.PHONY: all pull clean install update-puppet
