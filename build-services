#!/usr/bin/perl
# vim:set ai et sts=4 sw=4 tw=4 tw=0:
# Local Variables:
# mode:cperl
# cperl-indent-level:4
# End:

# Copyright © Stephen Gran 2009
# Copyright (c) 2009, 2014 Peter Palfrader
#
# Author: Stephen Gran <steve@lobefin.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, under version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

use strict;
use warnings;
use YAML;
use POSIX qw(strftime);
use File::Path;
use English;
use Getopt::Long;
use File::Basename;
use File::Temp qw(tempfile);

use FindBin;
use lib "$FindBin::Bin";
use lib "$FindBin::Bin/../dns-helpers";

use DSA::DNSHelpers;


my $USAGE = "$PROGRAM_NAME: Usage: $PROGRAM_NAME [--help] [--status-dir=<dir>] [--hostsfile=<file>] <servicesdir>\n";
my $PARAMS;
if (!GetOptions (
    '--help'                 => \$PARAMS->{'help'},
    '--hostsfile=s'          => \$PARAMS->{'hostsfile'},
    '--status-dir=s'         => \$PARAMS->{'statusdir'},
    )) {
    die($USAGE);
};
die "$USAGE" if (@ARGV != 1);
my $SNIPPETDIR = shift;

if ($PARAMS->{'help'}) {
    print $USAGE;
    print "builds bind zonefile snippets for Debian's auto-dns setup.\n";
    exit (0);
};

my $CONFIG = load_config(qw{outdir-auto default_ttl});
my $OUTDIR = $CONFIG->{'outdir-auto'};
my $STATUSDIR = $PARAMS->{'statusdir'} || 'status';
my $HOSTSFILE = $PARAMS->{'hostsfile'} || 'hosts.yaml';

my $ALIVE_CACHE = {};
sub check_host {
    my $service = shift;
    my $host = shift;

    return $ALIVE_CACHE->{$service}->{$host} if (defined $ALIVE_CACHE->{$service} &&
                                                 defined $ALIVE_CACHE->{$service}->{$host});

    my $res_ok = !( -e "$STATUSDIR/$host" || -e "$STATUSDIR/$host-$service");

    $ALIVE_CACHE->{$service} = {} unless defined $ALIVE_CACHE->{$service};
    $ALIVE_CACHE->{$service}->{$host} = $res_ok;
    return $res_ok;
}

# builds a snippet from hosts in block (e.g. "default").
#
# If none of these hosts works, build from hosts fallback.
#
# If, at the end, we still do not a non-empty set, use the primary block completely.
#
# %options:  skip-checks => 0  [if 1, skip OK check]
#            empty-ok    => 0  [if 1, do not fill up zone with all hosts if nonoe of them works]
# 
sub build_service {
    my $hostref      = shift;
    my $servicename  = shift;
    my $serviceref   = shift;
    my $blockname    = shift;
    my $fallbackname = shift;
    my %options      = @_;

    my $ret = '';
    for my $proto (qw(4 6)) {
        for my $srv (@{$serviceref->{'hosts'}->{$blockname}}) {
            next unless (defined($hostref->{'hosts'}->{$srv}->{$proto}));
            next unless ($options{'skip-checks'} || check_host($servicename, $srv));
            for my $addr (split(/\s*,\s*/, $hostref->{'hosts'}->{$srv}->{$proto})) {
                my $rr_type = ($proto == 4 ? 'A' : 'AAAA');
                $ret .= sprintf("\%s\t\t%s\tIN\t%s\t%-37s\t; %s\n",
                                '@',
                                get_ttl($serviceref, $rr_type),
                                $rr_type,
                                $addr,
                                $srv);
            }
        }
    }

    if ($ret eq '') {
        if ($options{'skip-checks'}) {
            die("Service $servicename is empty even with skip-checks\n");
        }
        if (defined $fallbackname && defined $serviceref->{'hosts'}->{$fallbackname}) {
            $ret = build_service($hostref, $servicename, $serviceref, $fallbackname, undef, %options, 'empty-ok' => 1);
        }
        if (!$options{'empty-ok'} && $ret eq '') {
            return build_service($hostref, $servicename, $serviceref, $blockname, undef, %options, 'skip-checks' => 1);
        }
    }

    return $ret;
}

sub get_ttl {
    my $serviceref   = shift;

    if (defined $serviceref->{'ttl'}) {
        return $serviceref->{'ttl'};
    } else {
        return $CONFIG->{'default_ttl'};
    }
}

sub write_snippet {
    my $hostref      = shift;
    my $servicename  = shift;
    my $serviceref   = shift;

    my $servicerecords = build_service($hostref, $servicename, $serviceref, 'default', 'hail_mary');

    my @names = ($servicename);
    push @names, @{$serviceref->{'aliases'}} if defined $serviceref->{'aliases'};

    my $zonedata = '';
    $zonedata .= "; **** $servicename\n";
    $zonedata .= "; *********************************\n";
    for my $name (@names) {
        $zonedata .= "\$ORIGIN $name.\n";
        $zonedata .= $servicerecords;
        $zonedata .= "\n";
    }

    #safe_write("$OUTDIR/$servicename", $zonedata);
    return $zonedata;
}


open (F, '<', $HOSTSFILE) or die "Arggh: hosts file $HOSTSFILE: $!\n";
my ($hostref, undef, undef) = Load(join "", (<F>));
close F;

unless ($PARAMS->{'create-named-conf'} || $PARAMS->{'print-filenames-only'}) {
    mkpath($OUTDIR);
}

my $zonedata = '';

opendir(DIR, "$SNIPPETDIR") or die ("Cannot opendir $SNIPPETDIR: $!\n");
for my $dirent (sort {$a cmp $b} readdir(DIR)) {
    next if $dirent =~ /^\./;
    next unless $dirent =~ /\.service$/;

    my $servicename = basename($dirent, ".service");
    my $filename = "$SNIPPETDIR/$dirent";

    open (F, '<', $filename) or die "Arggh: yamlfile $filename: $!\n";
    my ($zoneconfref, undef, undef) = Load(join "", (<F>));
    close F;

    $zonedata .= write_snippet($hostref, $servicename, $zoneconfref);
}
safe_write("$OUTDIR/all", $zonedata);
